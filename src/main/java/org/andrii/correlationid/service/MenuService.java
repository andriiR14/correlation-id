package org.andrii.correlationid.service;

import lombok.extern.slf4j.Slf4j;
import org.andrii.correlationid.config.correlation.CorrelationIdPropagationExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class MenuService {

    private final Executor executor = CorrelationIdPropagationExecutor
            .wrap(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));

    public List<String> getMenu() throws ExecutionException, InterruptedException {
        return CompletableFuture.supplyAsync(() -> {
                    log.info("Getting menu");
                    List<String> menu = new ArrayList<>();
                    menu.add("main");
                    menu.add("about");
                    return menu;
                }
                , executor).get();
    }
}
