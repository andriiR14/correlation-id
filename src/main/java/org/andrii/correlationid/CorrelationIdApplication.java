package org.andrii.correlationid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorrelationIdApplication {

	public static void main(String[] args) {
		SpringApplication.run(CorrelationIdApplication.class, args);
	}

}
