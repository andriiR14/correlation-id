package org.andrii.correlationid.controller;

import lombok.extern.slf4j.Slf4j;
import org.andrii.correlationid.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
public class MainController {

    @Autowired
    private MenuService service;

    @GetMapping("/menu")
    public ResponseEntity<?> getMenu() throws ExecutionException, InterruptedException {
        log.info("Request Started");

        List<String> menu = service.getMenu();

        log.info(("Request Finished"));
        return new ResponseEntity(menu, HttpStatus.OK);
    }
}
