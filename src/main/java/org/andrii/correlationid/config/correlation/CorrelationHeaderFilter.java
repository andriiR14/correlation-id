package org.andrii.correlationid.config.correlation;


import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

@Slf4j
@Component
public class CorrelationHeaderFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String currentCorrId = httpServletRequest.getHeader(RequestCorrelation.CORRELATION_ID);

        if (currentCorrId == null) {
            currentCorrId = UUID.randomUUID().toString();
            log.info("No correlationId found in Header. Generated : " + currentCorrId);
        } else {
            log.info("Found correlationId in Header : " + currentCorrId);
        }

        MDC.put(RequestCorrelation.CORRELATION_ID, currentCorrId);

        filterChain.doFilter(httpServletRequest, servletResponse);
    }
}
