package org.andrii.correlationid.config.correlation;

import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

//@Component
@RequiredArgsConstructor
public class CorrelationIdPropagationExecutor implements Executor {

    private final Executor delegate;

    public static Executor wrap(Executor executor) {
        return new CorrelationIdPropagationExecutor(executor);
    }

    @Override
    public void execute(Runnable command) {
        String correlation_id = MDC.get(RequestCorrelation.CORRELATION_ID);

        delegate.execute(() -> {
            try {
                MDC.put(RequestCorrelation.CORRELATION_ID, correlation_id);
                command.run();
            } finally {
                MDC.remove(RequestCorrelation.CORRELATION_ID);
            }
        });
    }
}
